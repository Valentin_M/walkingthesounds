import * as Mobilizing from '@mobilizing/library';
import { Script } from "./script.js";

// We need to run the script in a Mobilizing Context
// Build a simple function to instantiate the Context,
// instantiate the user script,
// add it as a component to the Context,
// and build a runner with it.
function run() {
    const context = new Mobilizing.Context();
    const script = new Script();
    context.addComponent(script);
    const runner = new Mobilizing.Runner( {context} );
}

// Then run it
run();
