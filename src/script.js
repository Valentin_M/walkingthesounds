import * as Mobilizing from '@mobilizing/library';
import { TraiteurAcceleration } from "./tds";

export class Script {

    constructor() {

        this.accelProcessor = new TraiteurAcceleration(128, 0.01);

        console.log(this.accelProcessor);

        this.debugDiv = document.createElement("div");
        this.debugDiv.style.position = "fixed";
        this.debugDiv.style.color = "white";
        this.debugDiv.style.width = "300px";
        this.debugDiv.style.height = "200px";
        this.debugDiv.style.fontSize = "20px";
        //this.debugDiv.style.backgroundColor = "black";
        this.debugDiv.style.top = 0;
        this.debugDiv.style.left = 0;

        this.soundFilesNames = [
            "Son_1.1-rythme.wav",
            "Son_2.2-Bosses-11s.wav",
            "Son_2.4-Bosses-13s.wav",
            "Son_2.1-Bosses-13s.wav",
            "Son_2.3-Bosses-13s.wav"
        ];
    }

    // called just after the constructor
    preLoad(loader) {

        this.soundFilesRequests = [];

        for (let i = 0; i < this.soundFilesNames.length; i++) {
            this.soundFilesRequests.push(loader.loadArrayBuffer({ "url": "assets/audio/" + this.soundFilesNames[i] }));
        }
        this.fontRequest = loader.loadArrayBuffer({ "url": "assets/fonts/Raleway-Regular.ttf" });
    }

    // called when preloaded elements are loaded
    setup() {
        this.renderer = new Mobilizing.three.RendererThree({
            "preserveDrawingBuffer": true,
            "powerPreference": "high-performance"
        });
        this.context.addComponent(this.renderer);
        document.body.appendChild(this.debugDiv);

        this.camera = new Mobilizing.three.Camera();
        this.camera.setAutoClearColor(false);
        this.renderer.addCamera(this.camera);

        //gyro cam control
        this.orientation = new Mobilizing.input.Orientation();
        this.context.addComponent(this.orientation);
        this.orientation.setup();

        this.gyroQuat = new Mobilizing.three.Quaternion();
        console.log(this.gyroQuat);

        const light = new Mobilizing.three.Light();
        console.log(light);
        this.renderer.addToCurrentScene(light);

        this.accel = new Mobilizing.input.Motion();
        this.accel = this.context.addComponent(this.accel);
        this.accel.setup();//set it up

        this.accel.events.on("acceleration", this.accelEvent.bind(this));

        setInterval(() => {
            this.debugDiv.innerHTML = this.accelProcessor.getAccelerationMoyenne("x") + "<br>";
            this.debugDiv.innerHTML += this.accelProcessor.getAccelerationMoyenne("y") + "<br>";
            this.debugDiv.innerHTML += this.accelProcessor.getAccelerationMoyenne("z");
        }, 1000);

        //this is a simple plane renderer in front of everything
        //with transparency. Rendering accumulation erases slowly the scene
        //producing a kind of trail effect
        this.fadingScreen = new Mobilizing.three.Plane({
            "width": 20,
            "height": 20,
            "material": "basic"
        });
        this.fadingScreen.material.setTransparent(true);
        this.fadingScreen.material.setColor(Mobilizing.three.Color.black);
        this.fadingScreen.material.setOpacity(.05);

        this.renderer.addToCurrentScene(this.fadingScreen);
        this.fadingScreen.transform.setRenderOrder(-1);

        //to understand where we're looking at
        const cube = new Mobilizing.three.Cube({
            "material": "basic",
            "segments": 8
        });
        cube.transform.setLocalPosition(0, 0, 0);
        cube.transform.setLocalScale(1000);
        cube.material.setWireframe(true);
        const c = .3;
        const color = new Mobilizing.three.Color(c, c, c);
        cube.material.setColor(color);
        this.renderer.addToCurrentScene(cube);

        //audio
        this.audioRenderer = new Mobilizing.audio.Renderer();
        this.context.addComponent(this.audioRenderer);

        //attach audio listener to 3D camera
        this.audioRenderer.setListenerTransform(this.camera.transform);

        this.soundSources = [];
        this.soundModels = [];

        for (let i = 0; i < this.soundFilesNames.length; i++) {

            const source = new Mobilizing.audio.Source({ "renderer": this.audioRenderer });
            this.context.addComponent(source);

            if (i !== 0) {
                source.set3D(true);
            }
            source.setLoop(true);
            source.setRefDistance(100);

            let sound = new Mobilizing.audio.Buffer({
                "renderer": this.audioRenderer,
                "arrayBuffer": this.soundFilesRequests[i].getValue(),
                "decodedCallback": () => {
                    source.setBuffer(sound);
                }
            });

            if (i !== 0) {
                const typeface = this.fontRequest.getValue();

                const soundModel = new Mobilizing.three.TextOpentype({
                    "fontFile": typeface,
                    "text": "S" + i,
                    "depth": 2,
                    "fontSize": 20,
                    //"material": "basic"
                });
                soundModel.transform.setLocalPositionZ(-100);
                this.renderer.addToCurrentScene(soundModel);

                source.setTransform(soundModel.transform);
                this.soundModels.push(soundModel);
            }

            this.soundSources.push(source);

        }

        window.addEventListener("touchend", () => {
            this.activateSensors();
            this.playAllSounds();
        });

        window.addEventListener("click", () => {
            this.activateSensors();
            this.playAllSounds();
        });
    }

    activateSensors() {
        this.accel.on();//active it
        this.orientation.on();
    }

    playAllSounds() {
        for (let i = 0; i < this.soundSources.length; i++) {
            this.soundSources[i].play();
        }
    }

    accelEvent(acc) {
        this.accelProcessor.ajouterEchantillon(acc.x, acc.y, acc.z);
    }

    // refresh loop
    update() {
        this.gyroQuat.setFromGyro(this.orientation.compass);

        if (this.gyroQuat) {
            this.camera.transform.setLocalQuaternion(this.gyroQuat);
        }

        for (let i = 0; i < this.soundModels.length; i++) {
            this.soundModels[i].transform.lookAt(this.camera.transform.getLocalPosition());
        }

        let factor = 4000;
        //3D objects moves
        this.soundModels[0].transform.setLocalPosition(100 * Math.cos(performance.now() / factor), 50 * Math.sin(performance.now() / factor), 0);

        factor = 4000;
        this.soundModels[1].transform.setLocalPosition(100 * Math.cos(performance.now() / factor), 50, 100 * Math.sin(performance.now() / factor));

        factor = 4500;
        this.soundModels[2].transform.setLocalPosition(100 * Math.cos(performance.now() / factor), 100 * Math.sin(performance.now() / factor), 100 * Math.sin(performance.now() / factor));

        factor = 4500;
        this.soundModels[3].transform.setLocalPosition(200 * Math.cos(performance.now() / factor), 50 * Math.sin(performance.now() / factor), 50 * Math.sin(performance.now() / factor));


    }
}
